# **Project CarCar** 🚘

## Team:

- **Nancy** - Service
- **Brandon** - Sales

## How to run Application:

1. Clone the repository https://gitlab.com/nancychavez/project-beta
2. Open Docker Desktop
3. Run the following commands to build Docker container
```
docker volume create beta-data
docker-compose build
docker-compose up
```
4. Make migrations
```
docker exec -it «api-container-name» bash
python manage.py makemigrations
python manage.py migrate
```
5. Create a super user
```
docker exec -it «api-container-name» bash
python manage.py createsuperuser
```
   * follow prompts in terminal
6. Go to http://localhost:3000 to access webpage

## Domain Driven Design:

![ddd](https://i.imgur.com/Fei4yi5.png)

## Service microservice

This service is an Automobile Service application that allows users to manage and track service appointments for automobiles and their owners. The front-end of the application includes forms for creating and managing service appointments, as well as a list of scheduled appointments. The back-end code supports the creation of automotive technicians and service appointments, as well as storing and retrieving service appointment information. The application includes forms for entering a technician's name and employee number and a service appointment, which are accessible via links in the navbar. Additionally, the application includes a list of scheduled appointments, a page that shows the service history for a specific VIN, and a button that allows a service concierge to cancel an appointment or to mark it as finished. The service also has an integration point with an Inventory microservice, where it checks if the VIN of a vehicle is for an automobile that was at one time in the inventory, so it could show that the automobile was purchased from the dealership.


## Sales microservice

This service is an Auto Sales application that allows users to track automobile sales that come from the inventory. The application has several features, including the ability to add a sales person, add a potential customer, create a sale record, list all sales, and list a sales person's sales history. The application also integrates with an Inventory microservice to ensure that a person cannot sell a car that is not listed in the inventory, nor can a person sell a car that has already been sold. The application includes forms for adding sales people and potential customers, as well as links in the navbar to access these forms, a page for listing all sales, and a page for listing a specific sales person's sales history. Overall, this service provides a way to track and manage the sales process for automobile inventory.


# Services

## Inventory

**Create New Manufacturer** | POST | http://localhost:8100/api/manufacturers/

* INPUT
```
{
  "name": "Jaguar"
}
```
* OUTPUT
```
{
	"href": "/api/manufacturers/9/",
	"id": 9,
	"name": "Jaguar"
}
```
<br><br>

**List Manufacturers** | GET | http://localhost:8100/api/manufacturers/ <br>

* OUTPUT
```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Porsche"
		},
		{
			"href": "/api/manufacturers/4/",
			"id": 4,
			"name": "Volvo"
		},
		{
			"href": "/api/manufacturers/5/",
			"id": 5,
			"name": "BMW"
		},
		{
			"href": "/api/manufacturers/6/",
			"id": 6,
			"name": "Honda"
		},
		{
			"href": "/api/manufacturers/8/",
			"id": 8,
			"name": "Subaru"
		},
		{
			"href": "/api/manufacturers/9/",
			"id": 9,
			"name": "Jaguar"
		}
	]
}
```
<br><br>

**Delete Manufacturer** | DELETE | http://localhost:8100/api/manufacturers/9/ <br>

* OUTPUT
```
{
	"id": null,
	"name": "Jaguar"
}
```

<br><br>

**Vehicle Model List** | GET | http://localhost:8100/api/models/ <br>

* OUTPUT
```
{
	"models": [
		{
			"href": "/api/models/6/",
			"id": 6,
			"name": "Panamera",
			"picture_url": "https://cdn.motor1.com/images/mgl/qp4ZZ/s3/porsche-panamera-vs-porsche-taycan.webp",
			"manufacturer": {
				"href": "/api/manufacturers/3/",
				"id": 3,
				"name": "Porsche"
			}
		},
		{
			"href": "/api/models/7/",
			"id": 7,
			"name": "XC40",
			"picture_url": "https://www.digitaltrends.com/wp-content/uploads/2017/12/rg-2019-volvo-xc40-first-drive-front-angle.jpg?resize=625%2C417&p=1",
			"manufacturer": {
				"href": "/api/manufacturers/4/",
				"id": 4,
				"name": "Volvo"
			}
		},
		{
			"href": "/api/models/9/",
			"id": 9,
			"name": "Civic",
			"picture_url": "https://media.ed.edmunds-media.com/honda/civic/2023/oem/2023_honda_civic_sedan_si_fq_oem_1_815.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/6/",
				"id": 6,
				"name": "Honda"
			}
		},
		{
			"href": "/api/models/11/",
			"id": 11,
			"name": "F-Type",
			"picture_url": "https://di-uploads-pod6.dealerinspire.com/jaguarmissionviejo/uploads/2020/02/2020-Jaguar-F-TYPE-configuration.png",
			"manufacturer": {
				"href": "/api/manufacturers/7/",
				"id": 7,
				"name": "Jaguar"
			}
		}
	]
}
```
<br><br>

**Create Vehicle Model** | POST | http://localhost:8100/api/models/ <br>

* INPUT
```
{
  "name": "Accord",
  "picture_url": "https://www.motortrend.com/uploads/2022/02/2023-Honda-Accord-rendering-01.jpg?fit=around%7C875:492",
  "manufacturer_id": 6
}
```
* OUTPUT
```
{
	"href": "/api/models/10/",
	"id": 10,
	"name": "Accord",
	"picture_url": "https://www.motortrend.com/uploads/2022/02/2023-Honda-Accord-rendering-01.jpg?fit=around%7C875:492",
	"manufacturer": {
		"href": "/api/manufacturers/6/",
		"id": 6,
		"name": "Honda"
	}
}
```

<br><br>

**Delete Vehicle Model** | DELETE | http://localhost:8100/api/models/1/ <br>

* OUTPUT
```
{
	"id": null,
	"name": "911 GT3",
	"picture_url": "https://www.carscoops.com/wp-content/uploads/2021/02/Porsche-911-GT3-Configurator-1a.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Porsche"
	}
}
```

<br><br>

**Create Automobile** | POST | http://localhost:8100/api/automobiles/ <br>

* INPUT
```
{
  "color": "Papaya Metallic",
  "year": 2022,
  "vin": "1J4GZ78Y5PC574443",
  "model_id": 1
}
```

<br><br>

**List Automobiles** | GET | http://localhost:8100/api/automobiles/ <br>

* OUTPUT
```
{
	"autos": [
			{
			"href": "/api/automobiles/1J4GZ78Y5PC574443/",
			"id": 5,
			"color": "Papaya Metallic",
			"year": 2021,
			"vin": "1J4GZ78Y5PC574443",
			"model": {
				"href": "/api/models/6/",
				"id": 6,
				"name": "Panamera",
				"picture_url": "https://cdn.motor1.com/images/mgl/qp4ZZ/s3/porsche-panamera-vs-porsche-taycan.webp",
				"manufacturer": {
					"href": "/api/manufacturers/3/",
					"id": 3,
					"name": "Porsche"
				}
			}
		},
		{
			"href": "/api/automobiles/1J4GZ78Y5PC571111/",
			"id": 6,
			"color": "Black",
			"year": 2023,
			"vin": "1J4GZ78Y5PC571111",
			"model": {
				"href": "/api/models/9/",
				"id": 9,
				"name": "Civic",
				"picture_url": "https://media.ed.edmunds-media.com/honda/civic/2023/oem/2023_honda_civic_sedan_si_fq_oem_1_815.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/6/",
					"id": 6,
					"name": "Honda"
				}
			}
		},
		{
			"href": "/api/automobiles/1J4GZ78Y5PC566666/",
			"id": 8,
			"color": "Red",
			"year": 2023,
			"vin": "1J4GZ78Y5PC566666",
			"model": {
				"href": "/api/models/10/",
				"id": 10,
				"name": "Accord",
				"picture_url": "https://www.motortrend.com/uploads/2022/02/2023-Honda-Accord-rendering-01.jpg?fit=around%7C875:492",
				"manufacturer": {
					"href": "/api/manufacturers/6/",
					"id": 6,
					"name": "Honda"
				}
			}
		},
	]
}
```
<br><br>

**Create Vehicle Model** | POST | http://localhost:8100/api/models/ <br>

* INPUT
```
{
  "name": "Panamera",
  "picture_url": "https://cdn.motor1.com/images/mgl/qp4ZZ/s3/porsche-panamera-vs-porsche-taycan.webp",
  "manufacturer_id": 1
}
```

* OUTPUT
```
{
	"href": "/api/models/6/",
	"id": 1,
	"name": "Panamera",
	"picture_url": "https://cdn.motor1.com/images/mgl/qp4ZZ/s3/porsche-panamera-vs-porsche-taycan.webp",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Porsche"
}
```

## Customers

**Create New Customer** | POST | http://localhost:8090/api/customers/ <br>

* INPUT
```
{
	"name": "Nancy",
	"address": "123 Lazy St",
	"phone_number": "3109994840"
}
```
* OUTPUT
```
{
	"href": "/api/customers/2/",
	"name": "Nancy",
	"address": "123 Lazy St",
	"phone_number": "3109994840",
	"id": 2
}
```
<br><br>

**List Customers** | GET | http://localhost:8090/api/customers/ <br>

* OUTPUT
```
[
	{
		"href": "/api/customers/2/",
		"name": "Nancy",
		"address": "123 Lazy St",
		"phone_number": "3109994840",
		"id": 2
	}
]
```
<br><br>

**Delete Customer** | DELETE | http://localhost:8090/api/customers/2/ <br>

* OUTPUT
```
{
	"name": "Nancy",
	"address": "123 Lazy St",
	"phone_number": "3109994840",
	"id": null
}
```

## Sales Person

**Create New Sales Person** | POST | http://localhost:8090/api/salesperson/ <br>

* INPUT
```
{
	"name": "Nancy",
	"employee_number": 666
}
```
* OUTPUT
```
{
	"name": "Nancy",
	"employee_number": 666,
	"id": 8
}
```
<br><br>

**Sales Person List** | GET | http://localhost:8090/api/salesperson/ <br>

* OUTPUT
```
[
	{
		"name": "Nancy",
		"employee_number": 666,
		"id": 8
	}
]
```
<br><br>

**Delete Sales Person** | DELETE | http://localhost:8090/api/salesperson/8/ <br>

* OUTPUT
```
{
	"name": "Nancy",
	"employee_number": 666,
	"id": null
}
```

## Appointments
**List Appointments** | GET | http://localhost:8080/api/appointments/ <br>

* OUTPUT
```
{
	"appointments": [
		{
			"href": "/api/appointments/detail/6/",
			"id": 6,
			"vin": "1J4GZ78Y5PC574443",
			"owner": "Daniel Astle",
			"date_time": "2023-02-10T12:30:00+00:00",
			"technician": {
				"href": "/api/technicians/3/",
				"name": "Sassy",
				"id": 3,
				"employee_number": 999
			},
			"reason": "Oil change",
			"vip": true,
			"finished": false
		},
		{
			"href": "/api/appointments/detail/5/",
			"id": 5,
			"vin": "1J4GZ78Y5PC574666",
			"owner": "Hank Hill",
			"date_time": "2023-01-28T18:45:00+00:00",
			"technician": {
				"href": "/api/technicians/1/",
				"name": "Nancy",
				"id": 1,
				"employee_number": 666
			},
			"reason": "Tune up",
			"vip": true,
			"finished": true
		}
	]
}
```

## Technicians

**Create New Techician** | POST | http://localhost:8080/api/technicians/ <br>

* INPUT
```
{
	"name": "Sassy",
	"employee_number": 999
}
```
* OUTPUT
```
{
	"href": "/api/technicians/3/",
	"name": "Sassy",
	"id": 3,
	"employee_number": 999
}
```

<br><br>

**List Technicians** | GET | http://localhost:8080/api/technicians/ <br>

* OUTPUT
```
{
	"technicians": [
		{
			"href": "/api/technicians/1/",
			"name": "Nancy",
			"id": 1,
			"employee_number": 666
		},
		{
			"href": "/api/technicians/3/",
			"name": "Sassy",
			"id": 3,
			"employee_number": 999
		}
	]
}
```
<br><br>

**Techician Details** | GET | http://localhost:8080/api/technicians/1/ <br>

* OUTPUT
```
{
	"href": "/api/technicians/1/",
	"name": "Nancy",
	"id": 1,
	"employee_number": 666
}
```
