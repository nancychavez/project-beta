import React, {useEffect, useState} from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleModelForm from './VehicleModelForm';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import ManufacturerList from './ListManufacturers';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './ListVehicleModels';
import AppointmentForm from './AppointmentForm';
import AppointmentsList from './AppointmentList';
import TechnicianForm from './TechnicianForm';
import ServiceHistory from './ServiceHistory';
import NewSale from './NewSale';
import ListSales from './ListSales';
import NewCustomer from './NewCustomer';
import NewSalesperson from './NewSalesperson';



function App() {

  const [manufacturers, setManufacturers] = useState([]);
  const fetchManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
      }
  }

  const [models, setModels] = useState([]);
  const fetchModels = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models)
      }
  }

  const [automobiles, setAutomobiles] = useState([]);
  const fetchAutomobiles = async () => {
    const url = 'http://localhost:8090/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data)
      }
  }

  const [salespersons, setSalesperson] = useState([]);
  const fetchSalesperson = async () => {
    const url = 'http://localhost:8090/api/salesperson/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalesperson(data)
      }
  }

  const [customers, setCustomer] = useState([]);
  const fetchCustomer = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomer(data)
      }
  }

  const [sales, setSales] = useState([]);
  const fetchSales = async () => {
    const url = 'http://localhost:8090/api/SaleHistory/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSales(data)
      }
  }



  useEffect(() => {
    fetchManufacturers();
    fetchModels();
    fetchAutomobiles();
    fetchSalesperson();
    fetchCustomer();
    fetchSales();
  }, []);



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians/new/" element={<TechnicianForm />} />
          <Route path="appointments/" element={<AppointmentsList />} />
					<Route path="appointments/new" element={<AppointmentForm />} />
          <Route path="history/" element={<ServiceHistory />} />
          <Route path="/automobiles/" element={<AutomobileList />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
          <Route path="models/">
          <Route path="" element={<VehicleModelList models={models}/>} />
					<Route path="new/" element={<VehicleModelForm />} />
					</Route>
          <Route path="manufacturers" >
          <Route path="" element={<ManufacturerList manufacturers={manufacturers} />} />
          <Route path="new" element={<ManufacturerForm/>} fetchManufacturers={fetchManufacturers}/>
          </Route>
          <Route path="/sales" element={<AppointmentsList />} />
          <Route path="/sales/new" element={<NewSale automobiles={automobiles} salespersons={salespersons} customers={customers}/>} />
          <Route path="/salesperson/new" element={<NewSalesperson salespersons={salespersons}/>} />
          <Route path="/customer/new" element={<NewCustomer customers={customers}/>} />
          <Route path="/salesperson/sales" element={<ListSales salespersons={salespersons} sales={sales}/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
