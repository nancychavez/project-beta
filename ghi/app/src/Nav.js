import { NavLink } from 'react-router-dom';



function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/">
                  Home
                </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new">
                New Manufacturer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">
                Manufacturer List
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="models/new">
                New Vehicle Model
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">
                Vehicle Model List
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="automobiles/new">
                New Automobile
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="automobiles/">
                List of Automobiles
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="appointments/new">
                New Service Appointment
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="appointments/">
                Appointment List
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="history/">
                Service History by VIN
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="technicians/new">
                New Technician
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customer/new">
                New Customer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/new">
                New Sales Record
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/SaleHistory">
                List of Sales
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salesperson/new">
                New Sales Person
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
