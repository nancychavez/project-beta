from django.urls import path
from .views import (
    api_customer,
    api_show_customer,
    api_list_history,
    api_show_history,
    api_list_automobiles,
    api_sales_person,
    api_show_salesperson
)

urlpatterns = [
    path("salesperson/", api_sales_person, name="api_sales_person"),
    path("salesperson/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),
    path("customers/", api_customer, name="api_customer"),
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"),
    path("SaleHistory/", api_list_history, name="api_list_history"),
    path("SaleHistory/<int:pk>/", api_show_history, name="api_show_history"),
    path("automobiles/", api_list_automobiles, name="api_list_automobile"),
]
